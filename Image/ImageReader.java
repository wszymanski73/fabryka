package Image;

public class ImageReader {
    public enum ImageType {
        PNG, JPG
    }

    public Image getImage(ImageType type) {
        switch (type) {
            case JPG:
                return new JpgImage();
            case PNG:
                return new PngImage();
        }
        throw new IllegalArgumentException("Unsupported image type!");
    }
}
