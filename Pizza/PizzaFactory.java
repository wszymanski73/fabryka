package Pizza;

public class PizzaFactory {
    public enum PizzaType {
        HAM_MUSHROOM,
        DELUXE,
        HAWAIIAN
    }

    public Pizza createPizza(PizzaType pizzaType) {
        switch (pizzaType) {
            case HAM_MUSHROOM:
                return new HamAndMushroomPizza();
            case DELUXE:
                return new DeluxePizza();
            case HAWAIIAN:
                return new HawaiianPizza();
        }
        throw new IllegalArgumentException("The pizza type " + pizzaType + " is not recognized.");
    }
}

